/**
 * @module Fistor
 * @desc module Fistor - files storage
 */
const path = require('path')
const init = require('./init')
const levelDB = require('./levelDB')
const remoteDB = require('./remoteDB')

const config = require('./config')
const util = require('./util')
const dbUtil = require('./db-util')

let isDebug = false

const setOptions = (opts = {}) => {
  isDebug = opts.debug
}

const debugLog = (...args) => {
  console.log(...args)
}

init(config)

/**
 * getFileStream
 * @param {string} ref file identifier
 * @param {object} opts
 * @return {stream} fileStream
 */
const getFileStream = (ref, opts = { ignoreDuplicate: false }) => {
  isDebug && debugLog('getFileStream -> ref  with opts : ', ref, opts)
  return dbUtil.getBy('ref', ref, remoteDB)
  .then(rec => {
    isDebug && debugLog(`getFileStream -> rec is ${!!rec} && rec.id = `, rec && rec.id)
    if (!opts.ignoreDuplicate && rec.duplicate && rec.ref) {
      isDebug && debugLog('getFileStream -> duplicate rec : ', rec.ref.id, ref)
      return dbUtil.getBy('id', rec.ref.id, remoteDB)
    }
    isDebug && debugLog('getFileStream -> original rec : ', rec.id, ref)
    return rec
  })
  .then(rec => {
    isDebug && debugLog('getFileStream -> rec state is : ', rec.state)
    if (rec && rec.state === 'complete') {
      return util.getFileStream(rec.id)
    } else {
      return false
    }
  })
  .catch(ex => {
    console.error('getFileStream error ::: ', ex)
    return false
  })
}

const getFileRec = (ref, opts = { ignoreDuplicate: false }) => {
  isDebug && debugLog('getFileRec -> ref with opts : ', ref, opts)
  return dbUtil.getBy('ref', ref)
  .then(rec => {
    isDebug && debugLog(`getFileRec -> rec is ${!!rec} && rec.id = `, rec && rec.id)
    if (!opts.ignoreDuplicate && rec.duplicate && rec.ref) {
      // console.log('duplicate rec : ', rec.ref.id, fileBase)
      return dbUtil.getBy('id', rec.ref.id)
    }
    // console.log('original rec : ', rec.id, fileBase)
    return rec
  })
  .then(rec => {
    if (rec && rec.state === 'complete') {
      return rec
    } else {
      return false
    }
  })
  .catch(ex => {
    console.error('getFileStream error ::: ', ex)
    return false
  })
}

/**
 * uploadFile
 * @desc upload file to storage
 * @param {string} file absolute file path
 * @param {object} metadata file metadata stored with file
 * @param {string} metadata.tag tag for make filename is unique if same filename with diff context will be upload. will be ignored if lenght > 12
 * @param {object} opts
 * @param {boolean} opts.ignoreUnique if 'true' file with the same checksum will be upload elsewhere will return link on existed file with the same checksum. 'false' by default
 * @param {boolean} opts.storeUniqueRef if 'true' file with will register in db with reference to original file record. 'true' by default. only make sense if ignoreUnique is 'false'
 */
const uploadFile = async function (source, metadata = {}, opts = { ignoreUnique: false, storeUniqueRef: true, overwrite: false }) {
  if (!metadata.ref || typeof metadata.ref !== 'string') {
    return {
      error: 'metadata.ref must be defined and must be string'
    }
  }
  const fileData = util.getFileId(metadata)
  fileData.cursor = util.getCursor()
  fileData.dest = path.join(config.dataPath, fileData.cursor, fileData.sha1 + '.file')
  fileData.id = `${fileData.cursor}/${fileData.sha1}`
  try {
    let msg = []
    let db = await levelDB()
    let existed = await dbUtil.getBy(util.getKey('ref', fileData.ref))
    if (existed && existed.state === 'complete') {
      if (opts.overwrite) {
        fileData.dest = existed.dest
        fileData.sha1 = existed.sha1
      } else {
        return {
          error: 'existed file',
          ref: existed
        }
      }
    }
    await db.batch()
      .put(util.getKey('id', fileData.id), fileData)
      .put(util.getKey('ref', fileData.ref), fileData)
      .write()
    let checksum = await util.getChecksumFile(source)
    msg.push(`checksum ok`)
    let foundSameFile = await dbUtil.getBy('checksum', checksum)
    msg.push(`sameFile ${!!foundSameFile}`)
    if (foundSameFile && !opts.ignoreUnique) {
      if (opts.storeUniqueRef) {
        msg.push(`storeUniqueRef ${opts.storeUniqueRef}`)
        await db.put(util.getKey('ref', fileData.ref), { ref: foundSameFile, duplicate: true })
        console.log(msg.join(' | '))
        return { ref: foundSameFile, duplicate: true }
      } else {
        foundSameFile.error = new Error(`file content is not unique, fileData : ${foundSameFile.ref},${foundSameFile.id} / ${fileData.ref}`)
        console.log(msg.join(' | '))
        return foundSameFile
      }
    } else {
      fileData.checksum = checksum
      await util.copyFile(source, fileData.dest)
    }

    fileData.metadata.updated_at = Date.now()
    fileData.state = 'complete'
    msg.push(`state complete`)

    db.batch()
      .put(util.getKey('id', fileData.id), fileData)
      .put(util.getKey('ref', fileData.ref), fileData)
      .put(util.getKey('checksum', fileData.checksum), fileData)
      .write()

    console.log(msg.join(' | '))
    return fileData
  } catch (ex) {
    fileData.error = ex
    return fileData
  }
}

module.exports = {
  setOptions,
  util,
  levelDB,
  dbUtil,
  getFileRec,
  getFileStream,
  uploadFile
}
