const level = require('level')
const path = require('path')
// const util = require('./util')
const config = require('./config')
const multilevel = require('multilevel')
const net = require('net')

const decode = (v) => {
  let value
  try {
    value = JSON.parse(v)
  } catch (ex) {
    value = v
  }
  return value
}

const internals = {
  dbInstance: false,
  checked: false,
  // keyEncoding: 'utf8',
  keyEncoding: {
    type: 'utf8',
    encode: (n) => n,
    decode: (v) => {
      v = v.split(':')
      let r = {}
      r[v.shift()] = v.join(':')
      console.log('r -> ', r)
      return r
    },
    buffer: false
  },
  // valueEncoding: 'json'
  valueEncoding: {
    type: 'utf8',
    encode: (n) => JSON.stringify(n),
    decode: decode,
    buffer: false
  }
}

// const checkAndRepair = () => {
//   return util.getFilesList(path.join(config.dataPath, '**/*.file'))
//   .then(files => files.map(el => {
//     let split = el.split('/')
//     let id = split.pop()
//     return `${split.pop()}/${id}`
//   }))
// }

const db = multilevel.client()
const con = net.connect(config.server.port || 3299)
con.pipe(db.createRpcStream()).pipe(con)

const remoteLevelDb = () => Promise.resolve(db)

const levelDb = function () {
  return new Promise((resolve, reject) => {
    if (!internals.dbInstance) {
      console.log('levelDB database path : ', path.join(config.dataPath, './mydb'))
      internals.dbInstance = level(path.join(config.dataPath, './mydb'), { keyEncoding: internals.keyEncoding, valueEncoding: internals.valueEncoding }, (err, db) => {
        if (err) {
          return reject(err)
        }
      })
    }
    return resolve(internals.dbInstance)
  })
  // .then(async db => {
  //   let ids = await checkAndRepair()
  //   const batch = db.batch()
  //   ids.forEach()
  // })
}

module.exports = {
  levelDb,
  remoteLevelDb
}
