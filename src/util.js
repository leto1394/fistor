const path = require('path')
const fs = require('fs')
const glob = require('glob')
const crypto = require('crypto')
const config = require('./config')
const ReadableStream = require('stream').Readable

const isStream = stream =>
  stream !== null &&
  typeof stream === 'object' &&
  typeof stream.pipe === 'function'

const isStreamReadable = stream =>
  isStream(stream) &&
  stream.readable !== false &&
  typeof stream._read === 'function' &&
  typeof stream._readableState === 'object'

const normalizeSource = (source) => {
  if (Buffer.isBuffer(source)) {
    let stream = new ReadableStream()
    stream.push(source)
    stream.push(null)
    return stream
  }
  return isStream(source) && isStreamReadable(source) ? source : fs.createReadStream(source)
}

const getFileId = (metadata) => {
  let random = Math.round(Math.random() * 10000)
  let data = {
    created_at: Date.now(),
    state: 'incomplete',
    ref: metadata.ref,
    metadata: metadata
  }
  data.sha1 = getSha1(`${data.created_at}-${random}-${data.ref}`)
  return data
}

const getCursor = function (cursor) {
  this.cursor = this.cursor || 0
  if (cursor) {
    this.cursor = cursor
  }
  let cur = this.cursor.toString(16)
  this.cursor += 1
  if (this.cursor > 1023) {
    this.cursor = 0
  }
  return cur
}

const getSha1 = function (data) {
  var generator = crypto.createHash('sha1')
  generator.update(data)
  return generator.digest('hex')
}

const getKey = (k, v) => `${k}:${v}`

const getChecksumFile = function (source) {
  return new Promise(function (resolve, reject) {
    const hash = crypto.createHash('md5')
    const rejectCleanup = function (err) {
      rd.destroy()
      reject(err)
    }
    const rd = normalizeSource(source)
    rd.on('data', function (data) {
      hash.update(data, 'utf8')
    })
    rd.on('end', function () {
      resolve(hash.digest('hex'))
    })
    rd.on('error', rejectCleanup)
  })
}

/**
 * @param {string|readStream} source 
 * @param {*} target 
 */
const copyFile = function (source, target) {
  return new Promise(function (resolve, reject) {
    const rejectCleanup = function (err) {
      rd.destroy()
      wr.end()
      reject(err)
    }
    const rd = normalizeSource(source)
    rd.on('error', rejectCleanup)
    const wr = fs.createWriteStream(target)
    wr.on('error', rejectCleanup)
    wr.on('finish', () => {
      resolve()
    })
    rd.pipe(wr)
  })
}

const getFileStream = (fid) => {
  let filePath = path.join(config.dataPath, fid + '.file')
  return new Promise((resolve, reject) => {
    fs.open(filePath, 'r', (err) => {
      if (err) {
        return reject(err)
      }
      return resolve(fs.createReadStream(filePath))
    })
  })
}

const getFilesList = (filePath) => {
  return new Promise((resolve, reject) => {
    glob(path.resolve(filePath), (error, files) => {
      if (error) {
        return reject(error)
      }
      resolve(files.map(el => path.resolve(el)))
    })
  })
}

module.exports = {
  isStream,
  isStreamReadable,
  getFilesList,
  getSha1,
  copyFile,
  getFileStream,
  getChecksumFile,
  getKey,
  getFileId,
  getCursor
}
