const path = require('path')
const config = require('./config')

const multilevel = require('multilevel')
const net = require('net')
const level = require('level')

const decode = (v) => {
  let value
  try {
    value = JSON.parse(v)
  } catch (ex) {
    value = v
  }
  return value
}

const internals = {
  dbInstance: false,
  checked: false,
  // keyEncoding: 'utf8',
  keyEncoding: {
    type: 'utf8',
    encode: (n) => n,
    decode: (v) => {
      v = v.split(':')
      let r = {}
      r[v.shift()] = v.join(':')
      console.log('r -> ', r)
      return r
    },
    buffer: false
  },
  // valueEncoding: 'json'
  valueEncoding: {
    type: 'utf8',
    encode: (n) => JSON.stringify(n),
    decode: decode,
    buffer: false
  }
}

const db = level(path.join(config.dataPath, './mydb'), { keyEncoding: internals.keyEncoding, valueEncoding: internals.valueEncoding })

net.createServer(function (con) {
  con.pipe(multilevel.server(db)).pipe(con)
}).listen(config.server.port || 3299)
