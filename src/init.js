const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const levelDB = require('./levelDB')
const cfg = require('./config')

const init = () => {
  try {
    fs.accessSync(cfg.dataPath)
  } catch (ex) {
    console.log('init data dir : ', cfg.dataPath)
    mkdirp.sync(cfg.dataPath)
  }

  for (let i = 0; i < 1024; i++) {
    let subfolder = path.join(cfg.dataPath, i.toString(16))
    try {
      fs.accessSync(subfolder)
      console.log('check subdir: ', subfolder)
    } catch (ex) {
      // console.log('ex for subfolder = ', subfolder, JSON.stringify(ex))
      if (ex.code === 'ENOENT') {
        // console.log('make subdir: ', subfolder)
        fs.mkdirSync(subfolder)
      }
    }
  }

  levelDB()
    .then((db) => {
      console.log('levelDB ready...')
    })
    .catch(error => {
      console.error('levelDB error : ', error)
      process.exit(1)
    })
}

module.exports = init
