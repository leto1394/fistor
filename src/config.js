const path = require('path')
// const fs = require('fs')

let cfg
try {
  cfg = require('../fistor.config.json')
} catch (ex) {
  cfg = {
    dataPath: path.join(process.env.HOME, '.fistor'),
    server: {
      port: 3299
    }
  }
}
cfg.server = cfg.server || {}
cfg.server.port = cfg.server.port || 3299

// try {
//   fs.accessSync(cfg.dataPath)
// } catch (ex) {
//   console.error('\nError ::: dataPath in fistor.config.json must be a absolute path\n')
//   process.exit(1)
// }

module.exports = cfg
