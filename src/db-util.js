const db = require('./levelDB')
const util = require('./util')

/**
 * return value if db contain key elsewhere return false
 * @param {*} k key
 * @param {*} v key value
 * @example getBy('ref', '5935') will check 'ref:5935' key existance
 */
const getBy = (k, v, dbConn) => {
  let key = v ? util.getKey(k, v) : k
  dbConn = dbConn || db()
  return dbConn
    .then(db => {
      return db.get(key)
    })
    .catch(() => false)
}

const getAllKeys = (f) => {
  return db()
  .then(db => {
    return new Promise((resolve, reject) => {
      // let result = []
      db.createReadStream()
        .on('data', (data) => {
          console.log('data = ', data)
        })
        .on('end', () => {

        })
    })
  })
}

module.exports = {
  getBy,
  getAllKeys
}
