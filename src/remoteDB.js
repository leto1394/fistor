const multilevel = require('multilevel')
const net = require('net')
const config = require('./config')

const db = multilevel.client()
const con = net.connect(config.server.port || 3299)
con.pipe(db.createRpcStream()).pipe(con)

const remoteLevelDb = () => Promise.resolve(db)

module.exports = remoteLevelDb
